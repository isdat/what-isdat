var items = document.querySelectorAll('li[data-id]');

items.forEach(function(item, index) {
    item.addEventListener("mouseenter", function() {
        var selectors_list = [],
            selector = "",
            matches;

        item.dataset.id.split(" ").forEach(function(value, index) {
            selectors_list.push('li[data-id~="' + value + '"]');
        });

        selector = selectors_list.join(",");

        matches = document.querySelectorAll(selector);

        matches.forEach(function(match, index) {
            match.classList.add('active');
        });

        var toRemoveArray = Array.prototype.slice.call(matches);
        var nodesArray = Array.prototype.slice.call(items);
        var toBlur = nodesArray.filter((i) => !toRemoveArray.includes(i))

        toBlur.forEach(function(match, index) {
            match.classList.add('blur');
        });
    });

    item.addEventListener("mouseleave", function() {
        items.forEach(function(item, index) {
            item.classList.remove('active', 'blur');
        });
    });
});
