var viewer = document.querySelector('.viewer');
var viewerCaption = document.querySelector('.viewer__caption');
var viewerBtnPrev = document.querySelector('.viewer__btn--prev');
var viewerBtnNext = document.querySelector('.viewer__btn--next');
var fig_elts = document.querySelectorAll('figure');


function activate(figure) {
    var img = figure.querySelector('img');
    var caption = figure.querySelector('figcaption');

    viewer.style.backgroundImage = "url('" + img.src + "')";
    viewerCaption.innerHTML = caption.innerHTML;

    fig_elts.forEach(function(item, index) {
        item.classList.remove('active');
    });
    figure.classList.add('active');
}

var observer = new IntersectionObserver(function(entries) {
	// isIntersecting is true when element and viewport are overlapping
	// isIntersecting is false when element and viewport don't overlap
    if(entries[0].isIntersecting === true) {
        activate(entries[0].target);
    } else {
        // var index = [...fig_elts].indexOf(entries[0].target);
        // activate(fig_elts[index - 1]);
    }
}, { threshold: [0] });


fig_elts.forEach(function(item, index) {
    item.addEventListener("mouseenter", function() {
        activate(item);
    });
    observer.observe(item);
});

viewerBtnNext.addEventListener("mouseup", function() {
    var active = document.querySelector('.active');

    var index = [...fig_elts].indexOf(active);
    activate(fig_elts[index + 1]);
    fig_elts[index + 1].scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
});


viewerBtnPrev.addEventListener("mouseup", function() {
    var active = document.querySelector('.active');

    var index = [...fig_elts].indexOf(active);
    activate(fig_elts[index - 1]);
    fig_elts[index - 1].scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});

});
